################################################################################################
# Apply from file (with/without) silencing errors
################################################################################################
resource "null_resource" "apply_from_file_with_error" {
  count = local.apply_with_error_from_file ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG    = var.kubeconfig
      MANIFEST_FILE = var.manifest_file
    }

    command = "kubectl apply -f $${MANIFEST_FILE}"
  }

  triggers = merge(local.triggers_detect_drift_from_file, {
    manifest_file = var.manifest_file # using the content of the file here would mean the file had to be available at terraform plan time
    external      = var.apply_trigger
  })
}

resource "null_resource" "apply_from_file_without_error" {
  count = local.apply_without_error_from_file ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG    = var.kubeconfig
      MANIFEST_FILE = var.manifest_file
    }

    command = "kubectl apply -f $${MANIFEST_FILE} || true"
  }

  triggers = merge(local.triggers_detect_drift_from_file, {
    manifest_file = var.manifest_file # using the content of the file here would mean the file had to be available at terraform plan time
    external      = var.apply_trigger
  })
}

################################################################################################
# Delete from file (with/without) silencing errors
################################################################################################
resource "null_resource" "delete_on_destroy_from_file_with_error" {
  count = local.delete_with_error_from_file ? 1 : 0

  provisioner "local-exec" {
    when = destroy

    environment = {
      KUBECONFIG    = self.triggers.kubeconfig
      MANIFEST_FILE = self.triggers.manifest
    }

    command = "kubectl delete -f $${MANIFEST_FILE}"
  }

  # Passing external values via triggers to workarround language limitation
  # https://github.com/hashicorp/terraform/issues/23679
  triggers = {
    kubeconfig = var.kubeconfig
    manifest   = var.manifest_file
  }
}

resource "null_resource" "delete_on_destroy_from_file_without_error" {
  count = local.delete_without_error_from_file ? 1 : 0

  provisioner "local-exec" {
    when = destroy

    environment = {
      KUBECONFIG    = self.triggers.kubeconfig
      MANIFEST_FILE = self.triggers.manifest
    }

    command = "kubectl delete -f $${MANIFEST_FILE} || true"
  }

  # Passing external values via triggers to workarround language limitation
  # https://github.com/hashicorp/terraform/issues/23679
  triggers = {
    kubeconfig = var.kubeconfig
    manifest   = var.manifest_file
  }
}
