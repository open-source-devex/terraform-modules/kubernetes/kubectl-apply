locals {
  apply_with_errors    = var.enabled && ! var.silence_apply_errors
  apply_without_errors = var.enabled && var.silence_apply_errors

  delete_with_errors    = var.enabled && var.enable_delete_on_destroy && ! var.silence_delete_errors
  delete_without_errors = var.enabled && var.enable_delete_on_destroy && var.silence_delete_errors

  # All possibilities for stdin
  apply_with_error_from_stdin     = local.apply_with_errors && var.stdin
  apply_without_error_from_stdin  = local.apply_without_errors && var.stdin
  delete_with_error_from_stdin    = local.delete_with_errors && var.stdin
  delete_without_error_from_stdin = local.delete_without_errors && var.stdin

  # All possibilities for file
  apply_with_error_from_file     = local.apply_with_errors && ! var.stdin
  apply_without_error_from_file  = local.apply_without_errors && ! var.stdin
  delete_with_error_from_file    = local.delete_with_errors && ! var.stdin
  delete_without_error_from_file = local.delete_without_errors && ! var.stdin
}
