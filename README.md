# kubectl-apply

Terraform module to run `kubectl apply` on `terraform apply` and `kubectl delete` on `terraform delete`.
The module offers two flavours of execution that can be set on apply and delete independently:
- producing errors, that means breaking the terraform run, if `kubectl` fails
- silencing errors even if `kubectl` fails

This module can automatically detect drift and trigger an `kubectl apply` in response to that.
In order to detect drift against running k8s clusters the module needs to be able to read the manifests to apply at `terraform plan` time.
That means the manifest needs to be read from data sources (that produce content during refresh), or the module needs to be applied only after resources that produce the manifest have applied (see terraform documentation on `-target`).

## Triggering

Currently the module triggers kubectl apply commands if
- resource was tainted in the terraform state
- input for the resource changed
- when `var.detect_drift` is set to `true` and the manifest is available during `terraform plan`, the module detects drift

## Usage

The simplest example reading the manifest from a file is this one 👇.
```hcl-terraform
module "apply_manifest" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply.git?ref=v1.0.3"

  enabled = true

  stdin         = false
  manifest_file = "./aws-k8s-cni.yaml"
  kubeconfig    = "./kube/config"
}
```

It's also possible, and the module's default behaviour, to get the manifest content directly from a variable.
```hcl-terraform
module "apply_manifest" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply.git?ref=v1.0.2"

  enabled = true

  stdin      = true
  manifest   = "---"
  kubeconfig = "./kube/config"
}
```

There are [examples](./examples) showing all the available options.

## Terraform docs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| external | n/a |
| null | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| apply\_trigger | Triggers for apply, whenever the string changes apply will trigger | `string` | `" "` | no |
| detect\_drift | Whether to try and detect drift to trigger apply | `bool` | `false` | no |
| enable\_delete\_on\_destroy | Whether to create a destroy time resource to run kubectl delete | `bool` | `false` | no |
| enabled | Whether to create the resources in the module | `bool` | `true` | no |
| kubeconfig | Value for KUBECONFIG on the kubectl environment (when `null` value is not set) | `string` | `null` | no |
| manifest | Content of the manifest to be supplied to kubectl via stdin | `string` | `null` | no |
| manifest\_file | Path to the manifest file to be supplied to kubectl via `-f` flag | `string` | `null` | no |
| silence\_apply\_errors | Whether the provisioners are configured to not fail even when the kubectl apply fails | `bool` | `true` | no |
| silence\_delete\_errors | Whether the provisioners are configured to not fail even when the kubectl delete fails | `bool` | `true` | no |
| stdin | Whether the manifest content is supplied in a variable (that gets piped to kubectl stdin) or a file. When set to false, `manifest_file` has to be set accordingly | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| apply\_triggers | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
