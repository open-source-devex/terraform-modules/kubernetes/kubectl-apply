locals {
  detect_drift_from_file  = var.enabled && var.detect_drift && ! var.stdin
  detect_drift_from_stdin = var.enabled && var.detect_drift && var.stdin

  detected_drift_from_file = try(data.external.detect_drift_from_file[0].result["changed"], "NA")
  triggers_detect_drift_from_file = local.detect_drift_from_file ? {
    # use of timestamp() makes sure resources are triggered even when they did not change since last rune
    drift = "drift detected :: ${local.detected_drift_from_file == "true" ? "true ${timestamp()}" : local.detected_drift_from_file}"
  } : {}

  detected_drift_from_stdin = try(data.external.detect_drift_from_stdin[0].result["changed"], "NA")
  triggers_detect_drift_from_stdin = local.detect_drift_from_stdin ? {
    # use of timestamp() makes sure resources are triggered even when they did not change since last rune
    drift = "drift detected :: ${local.detected_drift_from_stdin == "true" ? "true ${timestamp()}" : local.detected_drift_from_stdin}"
  } : {}
}

data "external" "detect_drift_from_file" {
  count = local.detect_drift_from_file ? 1 : 0

  program = ["${path.module}/scripts/diff-k8s-resource-from-file.sh"]

  query = {
    manifest_file = var.manifest_file
    kubeconfig    = var.kubeconfig
  }
}

data "external" "detect_drift_from_stdin" {
  count = local.detect_drift_from_stdin ? 1 : 0

  program = ["${path.module}/scripts/diff-k8s-resource-from-stdin.sh"]

  query = {
    manifest   = var.manifest
    kubeconfig = var.kubeconfig
  }
}
