# This module will not apply due to error

module "apply_manifest" {
  source = "../../"

  enabled = true

  manifest   = "---"
  kubeconfig = "./kube/config"

  silence_apply_errors  = false
  silence_delete_errors = false

  apply_trigger = "apply when this changes"
}
