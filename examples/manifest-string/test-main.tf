module "apply_manifest" {
  source = "../../"

  enabled = true

  manifest   = "---"
  kubeconfig = "./kube/config"

  silence_apply_errors  = true
  silence_delete_errors = true

  apply_trigger = "apply when this changes"
}
