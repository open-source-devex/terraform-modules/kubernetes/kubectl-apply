# This example needs two terraform runs
#   1. terraform apply -auto-approve -target local_file.manifest
#   2. terraform apply -auto-approve

resource "local_file" "manifest" {
  content  = "---"
  filename = "/tmp/manifest.yaml"
}

module "apply_manifest" {
  source = "../../"

  enabled = true

  stdin         = false
  manifest_file = local_file.manifest.filename
  kubeconfig    = "./kube/config"

  silence_apply_errors  = true
  silence_delete_errors = true

  apply_trigger = "apply when this changes"
}
