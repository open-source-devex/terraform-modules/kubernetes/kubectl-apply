output "apply_triggers" {
  # These resources should be mutually exclusive
  value = merge(
    try(null_resource.apply_from_file_with_error[0].triggers, {}),
    try(null_resource.apply_from_file_without_error[0].triggers, {}),
    try(null_resource.apply_from_stdin_with_error[0].triggers, {}),
    try(null_resource.apply_from_stdin_without_error[0].triggers, {}),
  )
}
