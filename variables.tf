variable "enabled" {
  description = "Whether to create the resources in the module"
  type        = bool
  default     = true
}

variable "kubeconfig" {
  description = "Value for KUBECONFIG on the kubectl environment (when `null` value is not set)"
  type        = string
  default     = null
}

variable "stdin" {
  description = "Whether the manifest content is supplied in a variable (that gets piped to kubectl stdin) or a file. When set to false, `manifest_file` has to be set accordingly"
  type        = bool
  default     = true
}

variable "manifest" {
  description = "Content of the manifest to be supplied to kubectl via stdin"
  type        = string
  default     = null
}

variable "manifest_file" {
  description = "Path to the manifest file to be supplied to kubectl via `-f` flag"
  type        = string
  default     = null
}

variable "apply_trigger" {
  description = "Triggers for apply, whenever the string changes apply will trigger"
  default     = " "
}

variable "silence_apply_errors" {
  description = "Whether the provisioners are configured to not fail even when the kubectl apply fails"
  type        = bool
  default     = true
}

variable "silence_delete_errors" {
  description = "Whether the provisioners are configured to not fail even when the kubectl delete fails"
  type        = bool
  default     = true
}

variable "enable_delete_on_destroy" {
  description = "Whether to create a destroy time resource to run kubectl delete"
  type        = bool
  default     = false
}

variable "detect_drift" {
  description = "Whether to try and detect drift to trigger apply"
  type        = bool
  default     = false
}
