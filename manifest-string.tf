################################################################################################
# Apply from stdin (with/without) silencing errors
################################################################################################
resource "null_resource" "apply_from_stdin_with_error" {
  count = local.apply_with_error_from_stdin ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig
      MANIFEST   = var.manifest
    }

    command = "echo \"$${MANIFEST}\" | kubectl apply -f -"
  }

  triggers = merge(local.triggers_detect_drift_from_stdin, {
    manifest = var.manifest
    external = var.apply_trigger
  })
}

resource "null_resource" "apply_from_stdin_without_error" {
  count = local.apply_without_error_from_stdin ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG    = var.kubeconfig
      MANIFEST_FILE = var.manifest_file
    }

    command = "echo \"$${MANIFEST}\" | kubectl apply -f - || true"
  }

  triggers = merge(local.triggers_detect_drift_from_stdin, {
    manifest = var.manifest
    external = var.apply_trigger
  })
}

################################################################################################
# Delete from stdin (with/without) silencing errors
################################################################################################
resource "null_resource" "delete_on_destroy_from_stdin_with_error" {
  count = local.delete_with_error_from_stdin ? 1 : 0

  provisioner "local-exec" {
    when = destroy

    environment = {
      KUBECONFIG = self.triggers.kubeconfig
      MANIFEST   = self.triggers.manifest
    }

    command = "echo \"$${MANIFEST}\" | kubectl delete -f - "
  }

  # Passing external values via triggers to workarround language limitation
  # https://github.com/hashicorp/terraform/issues/23679
  triggers = {
    kubeconfig = var.kubeconfig
    manifest   = var.manifest
  }
}

resource "null_resource" "delete_on_destroy_from_stdin_without_error" {
  count = local.delete_without_error_from_stdin ? 1 : 0

  provisioner "local-exec" {
    when = destroy

    environment = {
      KUBECONFIG = self.triggers.kubeconfig
      MANIFEST   = self.triggers.manifest
    }

    command = "echo \"$${MANIFEST}\" | kubectl delete -f - || true"
  }

  # Passing external values via triggers to workarround language limitation
  # https://github.com/hashicorp/terraform/issues/23679
  triggers = {
    kubeconfig = var.kubeconfig
    manifest   = var.manifest
  }
}
